package com.bitskeytechnologies.www.grocbits.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.bitskeytechnologies.www.grocbits.bizlayer.GrocBitsSession;

/**
 * Created by Mr.Neeraj on 11-06-2015.
 */
public class SMSTransaction extends BroadcastReceiver {
    private static final String ACTION_MMS_RECEIVED = "android.provider.Telephony.WAP_PUSH_RECEIVED";
    final SmsManager smsMgr = SmsManager.getDefault();

    String getSMSVerificationCode(String MessageBody)
    {
        String MessageFormat = GrocBitsSession.getSMSCodeFormat();
        /* TODO, doing it hardcoded for now */
        String Code = "1234";
        return Code;
    }


    @Override
    public void onReceive(Context context, Intent intent) {


        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    String SMSCode = this.getSMSVerificationCode(message);

                    //Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);


                    // Show Alert
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context,
                            "senderNum: "+ senderNum + ", message: " + message + "SMS Code:" + SMSCode, duration);
                    toast.show();

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);
        }
    }

}/* SMSTransaction */

