package com.bitskeytechnologies.www.grocbits;

import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Mr.Neeraj on 05-06-2015.
 */
public class StoreParentFragment extends android.support.v4.app.Fragment {

    public  static String getFragmentTag()
    {
        return "StoreParentFrag";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.store_parent_layout,container,false);
        InputMethodManager inputManager = (InputMethodManager) rootView
                .getContext()
                .getSystemService(rootView.getContext().INPUT_METHOD_SERVICE);
        IBinder binder = rootView.getWindowToken();
        inputManager.hideSoftInputFromWindow(binder,InputMethodManager.HIDE_NOT_ALWAYS);
        return rootView;
    }

}
