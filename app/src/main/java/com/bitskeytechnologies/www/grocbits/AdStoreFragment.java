package com.bitskeytechnologies.www.grocbits;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bitskeytechnologies.www.grocbits.bizlayer.GrocBitsBizAPI;

/**
 * Created by Mr.Neeraj on 05-06-2015.
 */
public class AdStoreFragment  extends GrocBaseFrag {

    public  static String getFragmentTag()
    {
        return "AdStoreFrag";
    }


    private View rootView = null;

    @Nullable
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.ad_store_layout,container,false);
        return rootView;
 }



    public void setImageForTab(int position)
    {
        String StoreType = GrocBitsBizAPI.getStoreTypeByPosition(position);
        if(StoreType == null)
            return;

        ImageView imgView = (ImageView) this.rootView.findViewById(R.id.adspace_image_view);
        if(imgView != null) {
            int ResourceId = GrocBitsBizAPI.GetImageDrawableId(StoreType);
            if (ResourceId >= 0)
                imgView.setBackgroundResource(ResourceId);
        }
        return;
    }

}
