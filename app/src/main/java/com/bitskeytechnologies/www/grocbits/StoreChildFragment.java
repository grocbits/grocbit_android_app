package com.bitskeytechnologies.www.grocbits;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bitskeytechnologies.www.grocbits.bizlayer.GrocBitsBizAPI;
import com.bitskeytechnologies.www.grocbits.views.SlidingTabLayout;

/**
 * Created by Mr.Neeraj on 05-06-2015.
 */
public class StoreChildFragment extends android.support.v4.app.Fragment  {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    OnHeadlineSelectedListener mCallback;

    public  static String getFragmentTag()
    {
        return "StoreChildFrag";
    }


    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }
    //private FragmentTabHost mTabHost;



    /**
     * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
     * The individual pages are simple and just display two lines of text. The important section of
     * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
     * {@link SlidingTabLayout}.
     */
    class StorePagerAdapter extends PagerAdapter {

        /**
         * @return the number of pages to display
         */
        @Override
        public int getCount()
        {
            //return 10;
            String []list = GrocBitsBizAPI.getStoreTypeListArray();
            return list.length;
        }

        /**
         * @return true if the value returned from {@link #instantiateItem(ViewGroup, int)} is the
         * same object as the {@link View} added to the {@link ViewPager}.
         */
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        // BEGIN_INCLUDE (pageradapter_getpagetitle)
        /**
         * Return the title of the item at {@code position}. This is important as what this method
         * returns is what is displayed in the {@link SlidingTabLayout}.
         * <p>
         * Here we construct one using the position value, but for real application the title should
         * refer to the item's contents.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            String []list = GrocBitsBizAPI.getStoreTypeListArray();
           // mCallback.onArticleSelected(position);
            return list[position];
//            return "Item " + (position + 1);
        }
        // END_INCLUDE (pageradapter_getpagetitle)

        /**
         * Instantiate the {@link View} which should be displayed at {@code position}. Here we
         * inflate a layout from the apps resources and then change the text view to signify the position.
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // Inflate a new layout from our resources
            View view = getActivity().getLayoutInflater().inflate(R.layout.store_view_pager,
                    container, false);
            // Add the newly created View to the ViewPager
            container.addView(view);


            // Retrieve a TextView from the inflated View, and update it's text
            TextView title = (TextView) view.findViewById(R.id.item_title);
            title.setText(GrocBitsBizAPI.getStoreTypeByPosition(position)+ ":" + String.valueOf(position + 1));

//            /* Change the AdSpace Image as well */
            mCallback.onArticleSelected(position);


            // Return the View
            return view;
        }

        /**
         * Destroy the item from the {@link ViewPager}. In our case this is simply removing the
         * {@link View}.
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);

        }

    }/*SamplePagerAdapter*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        return inflater.inflate(R.layout.child_store_layout,container,false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.store_viewpager);
        mViewPager.setAdapter(new StorePagerAdapter());

        mSlidingTabLayout = (SlidingTabLayout)view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
       // mViewPager.setOnPageChangeListener(myOnPageChangerListener);
                //mSlidingTabLayout.setOnPageChangeListener(new );
        //super.onViewCreated(view, savedInstanceState);

    }
//
//    ViewPager.OnPageChangeListener myOnPageChangerListener = new ViewPager.OnPageChangeListener() {
//        @Override
//        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            return ;
//
//        }
//
//        @Override
//        public void onPageSelected(int position) {
//
//            return;
//        }
//
//        @Override
//        public void onPageScrollStateChanged(int state) {
//
//            return;
//        }
//    };


}
