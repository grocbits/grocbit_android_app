package com.bitskeytechnologies.www.grocbits;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

/**
 * Created by Mr.Neeraj on 05-06-2015.
 */
public class LoginFragment extends GrocBaseFrag {
//    public class LoginFragment extends android.support.v4.app.Fragment {

    public static String getFragmentTag()
    {
        return "LoginFrag";
    }

    @Nullable
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView  = inflater.inflate(R.layout.login_layout,container,false);
        TextView tv = (TextView)rootView.findViewById(R.id.phone_number_ev);
        if(tv != null)
        {
//            tv.setKeyListener(new KeyListener() {
//                @Override
//                public int getInputType() {
//                    return 0;
//                }
//
//                @Override
//                public boolean onKeyDown(View view, Editable editable, int keyCode, KeyEvent keyEvent) {
//                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN)
//                            && (keyCode != KeyEvent.KEYCODE_ENTER))
//                    {
//                        TextView tv = (TextView)view;
//                        ProgressBar pBar = (ProgressBar)rootView.findViewById(R.id.login_progress);
//                        pBar.setProgress(tv.length()*10);
//                    }
//                    return false;
//                }
//
//                @Override
//                public boolean onKeyUp(View view, Editable editable, int i, KeyEvent keyEvent) {
//                    return false;
//                }
//
//                @Override
//                public boolean onKeyOther(View view, Editable editable, KeyEvent keyEvent) {
//                    return false;
//                }
//
//                @Override
//                public void clearMetaKeyState(View view, Editable editable, int i) {
//
//                }
//            });
            tv.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if(actionId== EditorInfo.IME_ACTION_DONE) {
//                        ProgressBar pBar = (ProgressBar)rootView.findViewById(R.id.login_progress);
//                        if( pBar != null)
//                        {
//                            pBar.setProgress(100);
//                        }
                        return false;
                    }
                      return false;
                }

            });

        }

//        ObjectAnimator animation = ObjectAnimator.ofInt(R.id.login_progress,"progress",1,500);
//        animation.setDuration (5000); //in milliseconds
//        animation.setInterpolator (new DecelerateInterpolator());
//        animation.start ();

        /*
        NOTE: Do not delete this code, we may  use it if we go for multiple countries.
        ***
        ****
        Spinner CountryNameSpinner = (Spinner)rootView.findViewById(R.id.countryname_spinner);
        ArrayAdapter<String> CountryNamesAdapter = new ArrayAdapter<String>(
                rootView.getContext(),android.R.layout.simple_spinner_item,CountryCode.getCountryNames(rootView.getContext()));
        CountryNamesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CountryNameSpinner.setAdapter(CountryNamesAdapter);


        Spinner CountryCodesSpinner = (Spinner)rootView.findViewById(R.id.countrycode_spinner);
        ArrayAdapter<String> CountryCodesAdapter = new ArrayAdapter<String>(
                rootView.getContext(),android.R.layout.simple_spinner_item,CountryCode.getCountryCodes(rootView.getContext()));
        CountryCodesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CountryCodesSpinner.setAdapter(CountryCodesAdapter);
        CountryCodesSpinner.setEnabled(false);


        //CountryNameSpinner.onSe
        Spinner countryid_spinner = (Spinner)rootView.findViewById(R.id.countryname_spinner);
        String CountrIdCodeArray[] = rootView.getResources().getStringArray(R.array.CountryCodes);
        //ArrayAdapter<CharSequence> adapter = ArrayAdapter<CharSequence>(CountrIdCodeArray);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                rootView.getContext(),android.R.layout.simple_spinner_item,CountrIdCodeArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryid_spinner.setAdapter(adapter);

        CountryCode.initializeCountryIdMaps(rootView.getContext());

        CountryCode.getCurrentCountryCode(rootView.getContext());
        int index = CountryCode.getCurrentCountryIndex(rootView.getContext());
        CountryCodesSpinner.setSelection(index);
        CountryNameSpinner.setSelection(index);
        countryid_spinner.setSelection(index);

        ***********************/
        return rootView;

 }

}
