package com.bitskeytechnologies.www.grocbits.bizlayer;

/**
 * Created by Mr.Neeraj on 11-06-2015.
 */
public class GrocBitsSession {
    private String sessionId = null;
    private String phoneNumber= null;
    private String countryCode = null;

    public GrocBitsSession(String phoneNumber, String countryCode) {
        this.phoneNumber = phoneNumber;
        this.countryCode = countryCode;
    }

    public boolean isSessionValid()
    {
        boolean result = false;
        /* TODO */
        return result;
    }

    /*
    Return String as SessionID
     */
    private String makeSessionWithServer(String smsCodeRecieved)
    {
        String lsessionID = null;
        /* TODO */
        return lsessionID;
    }

    private boolean validateSessionWithServer(String sessionId)
    {
        boolean result=false;
        /* TODO */
        return result;
    }

    /*
    It either returns new session Id or extend expiration time with the server
     */
    private String renewSessionWithServer(String sessionId)
    {
        String lsessionId = null;
        /* TODO */
        return lsessionId;
    }

    public static String getSMSCodeFormat()
    {
        /* TODO : Ideally this format must be returned by Server as we can manage the format from
        * server end in future, rather than changing the APP. For now making this format hardcoded*/
        String MessageFormat="Please use code <$$$$> to verify on GrocBits";
        return MessageFormat;
    }


}
