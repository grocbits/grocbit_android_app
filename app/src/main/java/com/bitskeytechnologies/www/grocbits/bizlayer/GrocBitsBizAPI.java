package com.bitskeytechnologies.www.grocbits.bizlayer;

import com.bitskeytechnologies.www.grocbits.R;

/**
 * Created by Mr.Neeraj on 09-06-2015.
 *
 * Note : All Rest API must be called in Service/Thread i.e. must be processed by Background Job.
 * For UI, till Rest API is completed, corresponding progress should be shown on it: TODO.
 */
public class GrocBitsBizAPI {

    static String []storeTypes = {"Grocery","Fresh Food", "Bakery","Pet Care", "Cosmetics"};

    /* This is not required to be RestAPI or call Rest API*/
    public static  String [] getDrawerListArray()
    {
        boolean isRegistered = false;
        boolean isLogin = false;
        String list[]=null;


        if(!isLogin)
        {
            list = new String[]{"Location","Login","My Address","My Orders","My Cart","Notification Center","Help","Call Us","Rate Us","Share","About Us"};
        }
        else
        {
            list = new String[]{"Location","My Address","My Orders","My Cart","Notification Center","Help","Call Us","Rate Us","Share","About Us","Logout"};
        }

        return list;
    }

    /* This must call Rest API & Populate Store Type Area based on the location provided
      TODO: This must get data from the Rest API:
     */
    public static  String [] getStoreTypeListArray()
    {
        boolean isRegistered = false;
        boolean isLogin = false;
        String list[]={"Grocery","Fresh Food", "Bakery","Pet Care", "Cosmetics"};
        return list;
    }

    public static String getStoreTypeByPosition(int position)
    {
        if(position > storeTypes.length)
            return null;
        else
            return storeTypes[position];
    }

    public static int GetImageDrawableId(String storeType)
    {
        int ResourceId = -1;
        if(storeType.equalsIgnoreCase("Grocery"))
        {
            ResourceId = R.drawable.grocery;
        }
        else if(storeType.equalsIgnoreCase("Fresh Food"))
        {
            ResourceId = R.drawable.fresh_food;
        }
        else if(storeType.equalsIgnoreCase("Pet Care"))
        {
            ResourceId = R.drawable.pet_care;

        }
        else if(storeType.equalsIgnoreCase("Cosmetics"))
        {
            ResourceId = R.drawable.cosmetics;
        }
        return ResourceId;
    }


}
