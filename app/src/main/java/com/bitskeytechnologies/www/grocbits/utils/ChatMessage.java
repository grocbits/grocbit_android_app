package com.bitskeytechnologies.www.grocbits.utils;

import com.bitskeytechnologies.www.grocbits.bizlayer.ChatDeliveryAck;

public class ChatMessage {
    public boolean left;
    public String message;
    ChatDeliveryAck deliveryAck = null;

    public ChatMessage(boolean left, String message) {
        super();
        this.left = left;
        this.message = message;
        deliveryAck = new ChatDeliveryAck();
    }

    public void updateDeliveryStatus(ChatDeliveryAck.DeliveryDestination status)
    {
        deliveryAck.setDeliveryDest(status);
    }
}