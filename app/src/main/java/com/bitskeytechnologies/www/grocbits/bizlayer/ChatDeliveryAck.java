package com.bitskeytechnologies.www.grocbits.bizlayer;

/**
 * Created by swarnkarn on 6/22/2015.
 */
public class ChatDeliveryAck {

    String ErrorMsg = null;
    public enum DeliveryDestination {DELIVERY_DESTINATION_NOT_KNOWN, DELIVERY_DESTINATION_TO_SERVER,DELIVERY_DESTINATION_TO_USER};
    DeliveryDestination deliveryDest = DeliveryDestination.DELIVERY_DESTINATION_NOT_KNOWN;

    public ChatDeliveryAck()
    {
       deliveryDest = DeliveryDestination.DELIVERY_DESTINATION_NOT_KNOWN;
    }

    public void setDeliveryDest(DeliveryDestination value)
    {
        deliveryDest = value;

    }
    public DeliveryDestination getDeliveryDest()
    {
        return  deliveryDest;
    }

    public void setErrorMessage(String error)
    {
        ErrorMsg = new String (error);
    }

    public String getErrorMessage()
    {
        return ErrorMsg;
    }
}
