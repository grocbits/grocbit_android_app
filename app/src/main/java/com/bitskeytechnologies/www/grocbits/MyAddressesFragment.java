package com.bitskeytechnologies.www.grocbits;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Mr.Neeraj on 05-06-2015.
 */
public class MyAddressesFragment  extends GrocBaseFrag  {

    public static String getFragmentTag()
    {
        return "AdStoreFrag";
    }

    @Nullable
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView  = inflater.inflate(R.layout.my_addresses_layout,container,false);
        List myAddressList = getMyAddressess();
        TextView warning_tv = (TextView)rootView.findViewById(R.id.myaddress_warning_msg);
        ImageView warning_img= (ImageView)rootView.findViewById(R.id.address_not_found_img);
        if(myAddressList == null)
        {
            warning_tv.setText("No My Addresses are added till now, Please add!!");
            warning_img.setBackgroundResource(R.drawable.address_found);
        }
        else
        {
            warning_img.setVisibility(View.INVISIBLE);
            warning_tv.setVisibility(View.INVISIBLE);
        }

        return rootView;

 }

    List getMyAddressess()
    {
        return null;
    }

}
