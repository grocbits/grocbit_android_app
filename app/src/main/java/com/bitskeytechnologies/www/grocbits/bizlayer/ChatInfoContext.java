package com.bitskeytechnologies.www.grocbits.bizlayer;

import android.util.Log;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * Created by swarnkarn on 6/22/2015.
 */
public class ChatInfoContext {

    XMPPTCPConnectionConfiguration.Builder configBuilder = null;
    private String FromUserName = null;
    private String FromUserPassword = null;
    String ServiceName = null;
    private String HostName = null;
    private String ToUserName = null;/* user to whom chatting in to be done */
    private boolean initializationInProgress = false;
    private boolean isLoginDone = false;
    private String loginError = null;
    ChatManager chatManager = null;
    private String lastLoginOfToUser = null;

    public ChatInfoContext(String fromUserName, String fromUserPassword, String serviceName, String hostName, String toUserName) {
        FromUserName = fromUserName;
        FromUserPassword = fromUserPassword;
        ServiceName = serviceName;
        HostName = hostName;
        ToUserName = toUserName;
    }

    AbstractXMPPConnection connection = null;


    void initializeChatConnection()
    {
        // Perform action on click
        new Thread(new Runnable() {
            public void run() {
                createChatConnection();
            }
        }).start();

    }

    public  ChatDeliveryAck sendChat(String ToUserName, String myMessage)
    {
        /*
        TODO : Need to find reliably the delivery status, whether to server or user.
        There is I think OpenFire Plugin Available for it, need to investigate.
         */
        ChatDeliveryAck ack = new ChatDeliveryAck();
        Chat chat = chatManager.createChat(ToUserName + "@" + ServiceName);
        try {
            chat.sendMessage(myMessage);
            ack.setDeliveryDest(ChatDeliveryAck.DeliveryDestination.DELIVERY_DESTINATION_TO_USER);
        } catch (SmackException.NotConnectedException e) {
             ack.setErrorMessage(e.getMessage());
            e.printStackTrace();
        }
        return ack;
    }

    void createChatConnection()
    {
        initializationInProgress = true;
        configBuilder = XMPPTCPConnectionConfiguration.builder();
//        configBuilder.setUsernameAndPassword("grocbit_user_1", "nswdel10");
        configBuilder.setUsernameAndPassword(FromUserName, FromUserPassword);
        configBuilder.setServiceName(ServiceName);
//        configBuilder.setServiceName("ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
        //configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible);
        configBuilder.setHost(HostName);
        //configBuilder.setHost("54.148.100.240");
        // configBuilder.setPort(5222);
        try {
            TLSUtils.acceptAllCertificates(configBuilder);
        }
        catch(Exception ex)
        {
            loginError = new String(ex.getMessage());
            Log.d("", loginError);
        }

        if(loginError != null )
            return;

        configBuilder.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });




        //configBuilder.setPort(5222);
        //configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        chatManager = null;

        connection = new XMPPTCPConnection(configBuilder.build());
// Connect to the server
        try
        {
            connection.connect();
            connection.login();
            //connection.login("amarnath","nswdel10");

            //connection.login();
        }
        catch(Exception ex)
        {
            loginError = new String(ex.getMessage());
            Log.d("", loginError);
        }

        if(loginError != null)
            return;

        if(connection.isConnected())
        {

            chatManager = ChatManager.getInstanceFor(connection);
            if(chatManager != null)
            {

                chatManager.addChatListener(new ChatManagerListener() {

                    @Override
                    public void chatCreated(Chat chat, boolean b) {
                        chat.addMessageListener(new ChatMessageListener() {
                            @Override
                            public void processMessage(Chat chat, Message message) {

//                        mServerResponse.gotMessage(message.getBody());
//                        Log.d(TAG, message.toString());
                            }
                        });
                    }
                });
            }

            //Chat chat2 = chatManager.createChat(USERNAME + "@" + DOMAIN);
            Chat chat2 = chatManager.createChat("amarnath@ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
            //Chat chat2 = chatManager.createChat("amarnath" + "@" + "54.148.100.240");

            try {
                chat2.sendMessage("Hi Amarnath, this is GrocBits");
            } catch (SmackException.NotConnectedException e) {
                String string = e.getMessage();

                e.printStackTrace();
            }


            //Chat chat2 = chatManager.createChat(USERNAME + "@" + DOMAIN);
            Chat chat3 = chatManager.createChat("manish@ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
            //Chat chat2 = chatManager.createChat("amarnath" + "@" + "54.148.100.240");

            try {
                chat3.sendMessage("Hi Manish, this is GrocBits");
            } catch (SmackException.NotConnectedException e) {
                String string = e.getMessage();

                e.printStackTrace();
            }
        }


// Disconnect from the server
        //connection.disconnect();
    }
}
