package com.bitskeytechnologies.www.grocbits;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.bitskeytechnologies.www.grocbits.utils.ChatArrayAdapter;
import com.bitskeytechnologies.www.grocbits.utils.ChatMessage;

/**
 * Created by Mr.Neeraj on 05-06-2015.
 */
public class ChatWindowFragment extends GrocBaseFrag {
//    public class LoginFragment extends android.support.v4.app.Fragment {
    private static final String TAG = "ChatActivity";

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText editText;
    private Button buttonSend;

    Intent intent;
    private boolean side = false;

    public static String getFragmentTag()
    {
        return "LoginFrag";
    }

    private boolean sendChatMessage(){
        chatArrayAdapter.add(new ChatMessage(side, editText.getText().toString()));
        editText.setText("");
        side = !side;
        return true;
    }

    @Nullable
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView  = inflater.inflate(R.layout.chat_win_layout,container,false);
        buttonSend = (Button) rootView.findViewById(R.id.buttonSend);
        listView = (ListView) rootView.findViewById(R.id.chat_listview);

        chatArrayAdapter  = new ChatArrayAdapter( getActivity(),R.layout.chat_window_list_layout);
        listView.setAdapter(chatArrayAdapter);

        editText = (EditText) rootView.findViewById(R.id.chatText);

        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
              sendChatMessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });


        return rootView;

 }

}
