package com.bitskeytechnologies.www.grocbits;

import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bitskeytechnologies.www.grocbits.bizlayer.GrocBitsBizAPI;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;


public class MainActivity extends ActionBarActivity implements StoreChildFragment.OnHeadlineSelectedListener{

    DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    String mTitle ="";

    public void onArticleSelected(int position) {
        // The user selected the headline of an article from the HeadlinesFragment
        // Do something here to display that article
        //String [] ltitles=  getResources().getStringArray(R.array.grocbits_array);
        AdStoreFragment adStoreFrag = (AdStoreFragment)
                getSupportFragmentManager().findFragmentByTag(AdStoreFragment.getFragmentTag());

        if(adStoreFrag != null)
            adStoreFrag.setImageForTab(position);

    }

    void setUpStoreFragment(String mTitle)
    {
        android.support.v4.app.FragmentManager managerF = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = managerF.beginTransaction();
        String CountryId = com.bitskeytechnologies.www.grocbits.utils.CountryCode.getCurrentCountryCodeIdFromSIM(this);



        if(mTitle.equalsIgnoreCase("Location"))
        {
            StoreParentFragment storeParentFrag = null;
            AdStoreFragment adStoreFragment = null;
            storeParentFrag = (StoreParentFragment)managerF.findFragmentByTag(StoreParentFragment.getFragmentTag());
            if(storeParentFrag == null)
                    storeParentFrag = new StoreParentFragment();
            adStoreFragment = (AdStoreFragment)managerF.findFragmentByTag(AdStoreFragment.getFragmentTag());
            if(adStoreFragment == null)
                adStoreFragment = new AdStoreFragment();
            StoreChildFragment cStoreFragment = new StoreChildFragment();
//            LoginFragment loginFragment  = (LoginFragment)managerF.findFragmentByTag(LoginFragment.getFragmentTag());
//            if(loginFragment == null)
//            loginFragment = new LoginFragment();
//            if(loginFragment != null)
//            {
//                ft.remove(loginFragment);
//            }
            ft.replace(R.id.content_frame,storeParentFrag,"StoreParentFrag");
//            ft.remove(adStoreFragment);
            ft.add(R.id.store_parent_layout, adStoreFragment,"AdStoreFrag");
            ft.add(R.id.store_parent_layout,cStoreFragment,"StoreChildFrag");

         //   ft.replace(R.id.store_parent_layout,cStoreFragment);
            //ft.add(R.id.store_parent_layout,storeFragment);
//            ft.replace(R.id.store_parent_layout,adStoreFragment);
//            ft.replace(R.id.store_parent_layout,storeFragment);
            ft.commit();
        } else if(mTitle.equalsIgnoreCase("Login")){
            //TODO We may not required to create fragment again & again!!
            StoreParentFragment storeParentFrag = new StoreParentFragment();
            LoginFragment loginFragment = new LoginFragment();
            ft.replace(R.id.content_frame,storeParentFrag);
            ft.add(R.id.store_parent_layout, loginFragment,"LoginFrag");
            ft.commit();
        }
        else if(mTitle.equalsIgnoreCase("My Address")){
            StoreParentFragment storeParentFrag = new StoreParentFragment();
            MyAddressesFragment myAddressFrag = new MyAddressesFragment();
            ft.replace(R.id.content_frame,storeParentFrag);
            ft.add(R.id.store_parent_layout, myAddressFrag,"MyAddressFrag");
            ft.commit();
        }
        else if(mTitle.equalsIgnoreCase("MyCart")){


        } else if(mTitle.equalsIgnoreCase("NotificationCenter")){

        } else if(mTitle.equalsIgnoreCase("Help")){

        }else if(mTitle.equalsIgnoreCase("CallUs")){

        } else if(mTitle.equalsIgnoreCase("RateUs")){

        }
        else if(mTitle.equalsIgnoreCase("My Orders")){

            //TODO We may not required to create fragment again & again!!
            StoreParentFragment storeParentFrag = new StoreParentFragment();
            ChatWindowFragment chatWindowFrag = new ChatWindowFragment();
            ft.replace(R.id.content_frame,storeParentFrag);
            ft.add(R.id.store_parent_layout, chatWindowFrag,"ChatWindowFrag");
            ft.commit();

        }


    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = (String) getTitle();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.drawable.ic_drawer,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("Select a Store");
                invalidateOptionsMenu();

            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        String [] DrawerItems = GrocBitsBizAPI.getDrawerListArray();
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//                getBaseContext(),R.layout.drawer_list_item,
//                getResources().getStringArray(R.array.grocbits_array));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getBaseContext(),R.layout.drawer_list_item,
                DrawerItems);


        mDrawerList.setAdapter(adapter);
//        android.app.ActionBar actionBar = getActionBar();
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null )
        {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
       // getActionBar().setHomeButtonEnabled(true);
       // getActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String grofer_items[] = getResources().getStringArray(R.array.grocbits_array);
                String grofer_items[] =  GrocBitsBizAPI.getDrawerListArray();
                mTitle = grofer_items[position];
                setUpStoreFragment(mTitle);
//                NavigationFragment frag = new NavigationFragment();
//                StoreParentFragment storeParentFrag = new StoreParentFragment();
//                Bundle data = new Bundle();
//                data.putInt("position",position);
//                frag.setArguments(data);
//                FragmentManager managerF = getFragmentManager();
//                FragmentTransaction ft = managerF.beginTransaction();
//                ft.replace(R.id.content_frame,storeParentFrag);
//                ft.replace(R.id.store_parent_layout,frag);
//                //ft.replace(R.id.content_frame,frag);
//                ft.commit();

                mDrawerLayout.closeDrawer(mDrawerList);

            }


        });

    }

//    void SetAdSpaceFragmentArea(String tag)
//    {
//        AdStoreFragment adStoreFrag  = (AdStoreFragment)getSupportFragmentManager().findFragmentById(R.id.ad_store_layout);
//        if(adStoreFrag != null)
//        {
//            if(tag.equalsIgnoreCase("Grocery"))
//            {
//                ImageView imgV = (ImageView)this.findViewById(R.id.adspace_image_view);
//                if(imgV != null)
//                {
//
//                }
//
//            }
//        }
//
//    }
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isDrawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!isDrawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if( mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void createChatConnection()
    {
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        configBuilder.setUsernameAndPassword("grocbit_user_1", "nswdel10");
        //configBuilder.setResource("Phone");
        // configBuilder.setServiceName("54.148.100.240");
        //configBuilder.setServiceName("myService");
        configBuilder.setServiceName("ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
        //configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible);
        configBuilder.setHost("54.148.100.240");
        // configBuilder.setPort(5222);
        try {
            TLSUtils.acceptAllCertificates(configBuilder);
        }
        catch(Exception ex)
        {
            String message = ex.getMessage();
            Log.d("", message);

        }

        configBuilder.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });




        //configBuilder.setPort(5222);
        //configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        ChatManager chatManager = null;

        AbstractXMPPConnection connection = new XMPPTCPConnection(configBuilder.build());
// Connect to the server
        try
        {
            connection.connect();
            connection.login();
            //connection.login("amarnath","nswdel10");

            //connection.login();
        }
        catch(Exception ex)
        {
            String msg = ex.getMessage();
            Log.d("Error",msg);
        }
        if(connection.isConnected())
        {

            chatManager = ChatManager.getInstanceFor(connection);
            if(chatManager != null)
            {

                chatManager.addChatListener(new ChatManagerListener() {

                    @Override
                    public void chatCreated(Chat chat, boolean b) {
                        chat.addMessageListener(new ChatMessageListener() {
                            @Override
                            public void processMessage(Chat chat, Message message) {
//                        mServerResponse.gotMessage(message.getBody());
//                        Log.d(TAG, message.toString());
                            }
                        });
                    }
                });
            }

            //Chat chat2 = chatManager.createChat(USERNAME + "@" + DOMAIN);
            Chat chat2 = chatManager.createChat("amarnath@ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
            //Chat chat2 = chatManager.createChat("amarnath" + "@" + "54.148.100.240");

            try {
                chat2.sendMessage("Hi Amarnath, this is GrocBits");
            } catch (SmackException.NotConnectedException e) {
                String string = e.getMessage();

                e.printStackTrace();
            }


            //Chat chat2 = chatManager.createChat(USERNAME + "@" + DOMAIN);
            Chat chat3 = chatManager.createChat("manish@ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
            //Chat chat2 = chatManager.createChat("amarnath" + "@" + "54.148.100.240");

            try {
                chat3.sendMessage("Hi Manish, this is GrocBits");
            } catch (SmackException.NotConnectedException e) {
                String string = e.getMessage();

                e.printStackTrace();
            }
        }


// Disconnect from the server
        //connection.disconnect();
    }


}
