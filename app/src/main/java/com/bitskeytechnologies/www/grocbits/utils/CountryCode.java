package com.bitskeytechnologies.www.grocbits.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.bitskeytechnologies.www.grocbits.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mr.Neeraj on 10-06-2015.
 */
public class CountryCode {

    public static Map gMapCountryIdCode = new HashMap();
    public static Map gMapCountryIdName = new HashMap();
    public static String[] gCountryNames = null;
    public static String[] gCountryCodes = null;
    public static String[] gCountryId = null;
    public static boolean gisCountryCallInitialized = false;

    public static void  initializeCountryIdMaps(Context mContext)
    {
        String[] CountryCodeId = mContext.getResources().getStringArray(R.array.CountryCodes);
        String[] CountryNamesId = mContext.getResources().getStringArray(R.array.CountryNamesId);
        int maxLength = CountryCodeId.length;
        gCountryNames = new String[maxLength];
        gCountryCodes = new String[maxLength];
        gCountryId  = new String[maxLength];

        for(int i=0;i<CountryCodeId.length;i++){
            //g[0]-->Country Code & g[1] is CountryId
            String[] g=CountryCodeId[i].split(",");
            gCountryCodes[i] = new String(g[0].trim());
            gCountryId[i]=new String(g[1].trim());
        }

        for(int i=0;i<CountryNamesId.length;i++){
            //g[0]-->Country Name & g[1] is CountryId
            String[] g=CountryNamesId[i].split(",");
            gMapCountryIdName.put(g[1].trim(),g[0].trim());
        }

        for(int i=0;i<maxLength;i++)
        {
            Object name = gMapCountryIdName.get(gCountryId[i]);
            if(name != null)
               gCountryNames[i] = new String(name.toString());
            else
                Log.d("Null OBJECT",":FOUND");

        }

        gisCountryCallInitialized= true;
    }

   public static String [] getCountryNames(Context mContext)
    {
        if(!gisCountryCallInitialized)
            initializeCountryIdMaps(mContext);
        return gCountryNames;
    }

    public static String [] getCountryCodes(Context mContext)
    {
        if(!gisCountryCallInitialized)
            initializeCountryIdMaps(mContext);
        return gCountryCodes;
    }


    public static String getCurrentCountryCodeIdFromSIM(Context mContext)
    {
        String CountryId = null;
        TelephonyManager manager = (TelephonyManager )mContext.getSystemService(mContext.TELEPHONY_SERVICE);
        CountryId= manager.getSimCountryIso().toUpperCase();
        return  CountryId;
    }

    public static String getCountryCodeByCountryId(String CountryId,Context mContext)
    {
        String CountryZipCode="";
        String[] rl=mContext.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryId.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public static String getCurrentCountryCode(Context mContext)
    {
        String CountryId = getCurrentCountryCodeIdFromSIM(mContext);
        String CountryZipCode = getCountryCodeByCountryId(CountryId,mContext);
        return CountryZipCode;
    }

    public static int getCountryIndexByCountryId(String CountryId,Context mContext)
    {
        String CountryZipCode="";
        int index = 0;
        String[] rl=mContext.getResources().getStringArray(R.array.CountryCodes);
        for(index=0;index<rl.length;index++){
            String[] g=rl[index].split(",");
            if(g[1].trim().equals(CountryId.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return index;
    }

    public static int getCurrentCountryIndex(Context mContext)
    {
        String CountryZipCode="";
        String CountryId=getCurrentCountryCodeIdFromSIM(mContext);
        int index = 0;
        String[] rl=mContext.getResources().getStringArray(R.array.CountryCodes);
        for(index=0;index<rl.length;index++){
            String[] g=rl[index].split(",");
            if(g[1].trim().equals(CountryId.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return index;
    }

//    public static int getCountryNameByCountryId()
//    {
//
//    }





}
